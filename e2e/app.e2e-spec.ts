import { NewP1Page } from './app.po';

describe('new-p1 App', () => {
  let page: NewP1Page;

  beforeEach(() => {
    page = new NewP1Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
