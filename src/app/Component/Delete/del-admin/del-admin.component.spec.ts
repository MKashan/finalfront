import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelAdminComponent } from './del-admin.component';

describe('DelAdminComponent', () => {
  let component: DelAdminComponent;
  let fixture: ComponentFixture<DelAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
