import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-del-admin',
  templateUrl: './del-admin.component.html',
  styleUrls: ['./del-admin.component.css']
})
export class DelAdminComponent implements OnInit {
  id: string;
  responce= {};
  toggle= false;
  message: string;
  admin=[];
  constructor(private authan: GlobalServService ){}
  ngOnInit() {
    this.toggle = false;
    this.authan.getAdmins().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.id=responce.data[0].id;
              this.admin= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.id= deviceValue.target.value;

  }
  delete() {

    this.authan.deleteAdmin(this.id).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message= responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }
}
