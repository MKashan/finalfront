import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelCampusComponent } from './del-campus.component';

describe('DelCampusComponent', () => {
  let component: DelCampusComponent;
  let fixture: ComponentFixture<DelCampusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelCampusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelCampusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
