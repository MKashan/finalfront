import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-del-campus',
  templateUrl: './del-campus.component.html',
  styleUrls: ['./del-campus.component.css']
})
export class DelCampusComponent implements OnInit {
  value: string;
  responce= {};
  Datas= [];
  toggle= false;
  message: string;
  campus=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.campus= responce.data;
              this.value=responce.data[0].id;

              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.value= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  delete(){
   console.log('sdsadasdsadasd',this.value)
    this.authan.deleteCampus(this.value).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

}
