import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-del-class',
  templateUrl: './del-class.component.html',
  styleUrls: ['./del-class.component.css']
})
export class DelClassComponent implements OnInit {
  campusId: string;
  secId: string;
  class: string;
  responce= {};
  Datas= [];
  toggle= false;
  message: string;
  section=[];
  campus=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.campusId=responce.data[0].id;
              this.campus= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.secId=responce.data[0].name;
              this.section= responce.data;
              this.class=responce.data[0].class;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  toggles() {
    this.toggle = false;
  }
  on(deviceValue) {
    this.class= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.campusId= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.secId= deviceValue.target.value;

  }
  delete(){

    this.authan.deleteSection(this.campusId,this.secId,this.class).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

}
