import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelFeeComponent } from './del-fee.component';

describe('DelFeeComponent', () => {
  let component: DelFeeComponent;
  let fixture: ComponentFixture<DelFeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelFeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelFeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
