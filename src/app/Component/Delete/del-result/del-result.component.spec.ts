import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelResultComponent } from './del-result.component';

describe('DelResultComponent', () => {
  let component: DelResultComponent;
  let fixture: ComponentFixture<DelResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
