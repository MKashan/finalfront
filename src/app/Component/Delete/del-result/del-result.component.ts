import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-del-result',
  templateUrl: './del-result.component.html',
  styleUrls: ['./del-result.component.css']
})
export class DelResultComponent implements OnInit {
  year: string;
  type: string;
  StudentId
  id: string;
  responce= {};
  toggle= false;
  message: string;
  secId: string;
  courseId: string;
  student=[];
  course=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.year='2009';
    this.authan.getStudents().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.StudentId=responce.data[0].id;
              this.student= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.courseId=responce.data[0].id;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );

  }
  on(deviceValue) {
    this.StudentId= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.year= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.courseId= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  delete() {

    this.authan.deleteResult(this.StudentId,this.type,this.year,this.courseId).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }


}
