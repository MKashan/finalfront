import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-del-staff',
  templateUrl: './del-staff.component.html',
  styleUrls: ['./del-staff.component.css']
})
export class DelStaffComponent implements OnInit {
  id: string;
  responce= {};
  toggle= false;
  message: string;
  staff=[];
  constructor(private authan: GlobalServService ){}
  ngOnInit() {
  this.toggle=false;
    this.authan.getStaffs().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.staff= responce.data;
              this.id=responce.data[0].id;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.id= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  delete() {

    this.authan.deleteStaff(this.id).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message= responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }
}
