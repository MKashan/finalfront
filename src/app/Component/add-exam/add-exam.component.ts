import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
import {document} from '@angular/platform-browser/src/facade/browser';
@Component({
  selector: 'app-add-exam',
  templateUrl: './add-exam.component.html',
  styleUrls: ['./add-exam.component.css']
})
export class AddExamComponent implements OnInit {
  data = new Data();
  id: string;
  toggle= false;
  message:string;
  Day:string;
  month:string;
  year:string;
  course=[];
  sec:number;
  section=[];
  cour:number;
  constructor(private  authan: GlobalServService) { }

  ngOnInit() {
    this.Day = document.getElementById('selectAccType').value;
    this.month = document.getElementById('selectAccType2').value;
    this.year = document.getElementById('selectAccType3').value;
    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.sec=responce.data[0].id;
              this.section= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.cour=responce.data[0].id;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }

  on(deviceValue) {
    this.Day= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.month= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.year= deviceValue.target.value;

  }
  on4(deviceValue) {
    this.sec= deviceValue.target.value;

  }
  on5(deviceValue) {
    this.course= deviceValue.target.value;

  }
  toggles(){
    this.toggle = false;
  }
  add() {
     this.data.sectionId=this.sec;
     this.data.courseId=this.cour;
    this.data.date=this.Day+"/"+this.month+"/"+this.year;
    this.authan.addExam(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message;

              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }



}
class Data {
sectionId:number;
courseId:number;
date:string;

}
