import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-campus',
  templateUrl: './campus.component.html',
  styleUrls: ['./campus.component.css']
})
export class CampusComponent implements OnInit {
  vCampus = true;
  addCampus = true;
  delCampus = true;
  updCampus = true;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    if(localStorage.getItem('type') == 'loginStudent'||localStorage.getItem('type') == 'loginTeacher'){
      this.addCampus = false;
      this.delCampus = false;
      this.updCampus = false;
    }
  }

}
