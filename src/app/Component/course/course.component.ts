import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  veiw=true;
  add=true;
  delete=true;
  update=true;
  corInsec=true;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    if(localStorage.getItem('type') == 'loginStudent'||localStorage.getItem('type') == 'loginTeacher'){
      this.add = false;
      this.delete = false;
      this.update = false;
      this.corInsec = false;

    }
  }

}
