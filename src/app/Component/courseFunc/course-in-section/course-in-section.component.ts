import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-course-in-section',
  templateUrl: './course-in-section.component.html',
  styleUrls: ['./course-in-section.component.css']
})
export class CourseInSectionComponent implements OnInit {

  sectionName:string;
  classId: string;
  courseName: string;
  responce= {};
  data= new Data();
  toggle= false;
  message: string;
  course=[];
  section=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.data.courseName=responce.data[0].name;
              this.data.class=responce.data[0].class;
              this.courseName=responce.data[0].name;
              this.classId=responce.data[0].class;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.data.sectionName=responce.data[0].name;
              this.sectionName=responce.data[0].name;
              this.section= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on2(deviceValue) {
    this.data.sectionName= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.data.courseName= deviceValue.target.value;

  }
  on(deviceValue) {
    this.data.class= deviceValue.target.value;

  }
  on4(deviceValue) {
    this.courseName= deviceValue.target.value;

  }
  on5(deviceValue) {
    this.classId= deviceValue.target.value;

  }
  on6(deviceValue) {
    this.sectionName= deviceValue.target.value;

  }
  toggles(){
    this.toggle= false;
  }
  add(){
    console.log('osamasss',this.data)
    this.authan.addCourseInSec(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;

              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }
  delete(){
    this.authan.deletCourseInSec(this.sectionName,this.classId,this.courseName).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );

  }


}
class Data{
  courseName:string;
  class:number;
  sectionName:string;
}
