import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {
  veiw=true;
  add=true;
  delete=true;
  edit=true;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    if(localStorage.getItem('type') == 'loginStudent'||localStorage.getItem('type') == 'loginTeacher'){
     this.add=false;
     this.delete=false;
      this.edit=false;
    }
  }

}
