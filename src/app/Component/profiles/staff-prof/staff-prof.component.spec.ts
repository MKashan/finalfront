import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffProfComponent } from './staff-prof.component';

describe('StaffProfComponent', () => {
  let component: StaffProfComponent;
  let fixture: ComponentFixture<StaffProfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffProfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffProfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
