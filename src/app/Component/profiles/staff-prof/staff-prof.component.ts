import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-staff-prof',
  templateUrl: './staff-prof.component.html',
  styleUrls: ['./staff-prof.component.css']
})
export class StaffProfComponent implements OnInit {
  data=new Data3();
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.data=this.authan.staffOrAdminDetails;
  }


}
class Data3{
  firstName:string;
  lastName:string;
  age:number;
  email:string;
  gender:number;
  contact:number;
  campuses: string [];

}
