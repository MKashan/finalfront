import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-student-prof',
  templateUrl: './student-prof.component.html',
  styleUrls: ['./student-prof.component.css']
})
export class StudentProfComponent implements OnInit {
data=new Data();
  constructor( private authan: GlobalServService) { }

  ngOnInit() {

     this.data=this.authan.studendtDetails;
  }

}

class Data{
  firstName:string;
  lastName:string;
  age:number;
  email:string;
  gender:number;
  contact:number;
  section: {};
}
