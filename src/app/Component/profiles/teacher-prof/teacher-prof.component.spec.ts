import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherProfComponent } from './teacher-prof.component';

describe('TeacherProfComponent', () => {
  let component: TeacherProfComponent;
  let fixture: ComponentFixture<TeacherProfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherProfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherProfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
