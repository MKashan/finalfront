import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../Services/global-serv.service';
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  veiw=true;
  add=true;
  delete=true;
  edit=true;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    if(localStorage.getItem('type') == 'loginStudent'){
      this.add=false;
      this.delete=false;
      this.edit=false;
    }
  }
}
