import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherInCampComponent } from './teacher-in-camp.component';

describe('TeacherInCampComponent', () => {
  let component: TeacherInCampComponent;
  let fixture: ComponentFixture<TeacherInCampComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherInCampComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherInCampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
