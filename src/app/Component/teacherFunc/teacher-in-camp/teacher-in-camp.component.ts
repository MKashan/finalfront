import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-teacher-in-camp',
  templateUrl: './teacher-in-camp.component.html',
  styleUrls: ['./teacher-in-camp.component.css']
})
export class TeacherInCampComponent implements OnInit {
campusId: string;
teacherId: string;
  campusId2: string;
  teacherId2: string;
  value: string;
  responce= {};
  data= {'campusId':0, 'teacherId':0};
  toggle= false;
  message: string;
  teacher=[];
  campus=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getteachers().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.data.teacherId=responce.data[0].id;
              this.teacher= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.data.campusId=responce.data[0].id;
              this.campus= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.data.campusId= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.data.teacherId= deviceValue.target.value;

  }
  toggles(){
    this.toggle= false;
  }
teacherCampus(){
    console.log('sdsadasd',this.data)
  this.authan.addTeacherInCamp(this.data).subscribe(
    responce => {
      if ( responce.status != null) {
        console.log(responce);
        switch ( responce.status) {
          case 200:
            this.toggle = true;
            this.message = responce.message;

            break;
          case 403:
            this.toggle = true;
            this.message = responce.message;
            break;
          case 404:
            this.toggle = true;

            this.message = responce.message;
            break;
          case 500:
            this.toggle = true;
            this.message = responce.message;
            break;


        }


      }
    }
  );
}

  delete(){
    this.authan.deletTeachCamp(this.data.teacherId,this.data.campusId).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              console.log('sdsdsd', responce.message);
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );

  }

}
