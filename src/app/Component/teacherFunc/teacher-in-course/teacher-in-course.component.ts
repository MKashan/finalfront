import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';

@Component({
  selector: 'app-teacher-in-course',
  templateUrl: './teacher-in-course.component.html',
  styleUrls: ['./teacher-in-course.component.css']
})
export class TeacherInCourseComponent implements OnInit {

  teacherId:string;
  classId: string;
  courseName: string;
  responce= {};
  data= new Data();
  toggle= false;
  message: string;
  teacher=[];
  course=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getteachers().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.data.teacherId=responce.data[0].id;
              this.teacherId=responce.data[0].id;
              this.teacher= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.data.courseName=responce.data[0].name;
              this.courseName=responce.data[0].name;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.data.teacherId= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.data.courseName= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.courseName= deviceValue.target.value;

  }
  on4(deviceValue) {
    this.teacherId= deviceValue.target.value;

  }
  toggles(){
    this.toggle= false;
  }
  teacherCourse(){
    this.authan.addTeacherInCourse(this.data).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;

              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;

            case 400:
              this.toggle = true;
              this.message = responce.message;
              break;


          }


        }
      }
    );
  }
  delete(){
    this.authan.deletTeachCourse(this.teacherId,this.classId,this.courseName).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
            case 400:
              this.toggle = true;
              this.message = responce.message;
              break;
          }

        }
      }
    );

  }


}
class Data {
  teacherId:number;
  courseName:string;
  class:number;
}
