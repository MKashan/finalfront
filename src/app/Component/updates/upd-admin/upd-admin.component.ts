import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-upd-admin',
  templateUrl: './upd-admin.component.html',
  styleUrls: ['./upd-admin.component.css']
})
export class UpdAdminComponent implements OnInit {

  toggle= false;
  message: string;
  data= {};
  staffID: number;
  Signal:boolean;
  updateToggle=false;
admin=[];
  constructor(private authan: GlobalServService) { }
  ngOnInit() {
    this.toggle = false;
    this.authan.getAdmins().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.staffID=responce.data[0].id;
              this.admin= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.staffID= deviceValue.target.value;

  }
  toggles(){
    this.toggle = false;
  }

  getData(){
    this.authan.getAdmin(this.staffID).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          console.log(responce.message);
          switch ( responce.status) {
            case 200:
              this.updateToggle = true;
              this.Signal = true;
              this.toggle = true;
              this.data = responce.data[0];

              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    if (this.Signal == true) {
      this.authan.updateAdmin(this.data,this.staffID).subscribe(
        resp => {
          console.log('dasdsadadsa',resp.status)
          if (resp.status != null) {
            switch (resp.status) {
              case 201:
                this.updateToggle=false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }

  }

}
