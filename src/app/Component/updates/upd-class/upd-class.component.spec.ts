import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdClassComponent } from './upd-class.component';

describe('UpdClassComponent', () => {
  let component: UpdClassComponent;
  let fixture: ComponentFixture<UpdClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
