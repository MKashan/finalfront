import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';

@Component({
  selector: 'app-upd-course',
  templateUrl: './upd-course.component.html',
  styleUrls: ['./upd-course.component.css']
})
export class UpdCourseComponent implements OnInit {

  toggle= false;
  message: string;
  data = new Datas();
  name: string;
  class: string;
  Signal: boolean;
  updateToggle =false;
  course=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {

    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.name=responce.data[0].name;
              this.class=responce.data[0].class;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  toggles() {
    this.toggle = true;
  }


  on2(deviceValue) {
    this.class= deviceValue.target.value;

  }
  on(deviceValue) {
    this.name= deviceValue.target.value;

  }

  getData() {

    this.authan.getCourse(this.name,this.class).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('osama',responce.status);
          switch ( responce.status) {
            case 200:
              this.Signal = true;
              this.updateToggle = true;
              this.toggle = true;
              this.data = responce.data[0];
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.data = responce.data;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    if (this.Signal == true) {
      this.authan.updateCourse(this.name,this.data, this.class).subscribe(
        resp => {
          console.log('update',this.name)
          if (resp.status != null) {
            switch (resp.status) {
              case 201:
                this.updateToggle=false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }

  }


}
class Datas {
  name: string;
  syllabus: string;
  class: string;
  teacherId: string;
}
