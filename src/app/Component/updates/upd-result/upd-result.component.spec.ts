import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdResultComponent } from './upd-result.component';

describe('UpdResultComponent', () => {
  let component: UpdResultComponent;
  let fixture: ComponentFixture<UpdResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
