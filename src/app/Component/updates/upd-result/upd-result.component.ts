import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-upd-result',
  templateUrl: './upd-result.component.html',
  styleUrls: ['./upd-result.component.css']
})
export class UpdResultComponent implements OnInit {
  toggle= false;
  message: string;
  data=new Datas();
  name: string;
  Signal:boolean;
  updateToggle=false;
  studId: string;
  courseId: string;
  studId2: string;
  courseId2: string;
  type: string;
  year: string;
  year2: string;
  course=[];
  student=[];
  temp1:number;
  temp2:number;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.year='2009';
    this.authan.getCourses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.courseId=responce.data[0].id;
              this.course= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getStudents().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.studId=responce.data[0].id;
              this.student= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.studId= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.year= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.courseId= deviceValue.target.value;

  }
  on5(deviceValue) {
    this.studId2= deviceValue.target.value;

  }
  on6(deviceValue) {
    this.courseId2= deviceValue.target.value;

  }
  toggles() {
    this.toggle = true;
  }

  getData() {
    this.authan.getResult(this.studId,this.type,this.year,this.courseId).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('dsd',responce.message);
          console.log('dsd',responce.status);
          switch ( responce.status) {
            case 200:
              this.Signal=true;
              this.updateToggle=true;
              this.toggle = true;
              this.data.marks = responce.data[0].marks;
              this.data.grade= responce.data[0].grade;
              this.data.type= responce.data[0].type;
              this.data.year= responce.data[0].year;
              this.data.studentId= responce.data[0].resStudentId;
              this.data.courseId= responce.data[0].resCourseId;
              this.temp1= responce.data[0].resStudentId;
              this.temp2= responce.data[0].resCourseId;
              this.courseId=responce.data[0].resCourseId;
              this.message= responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    if (this.Signal == true) {
 this.data.courseId=this.courseId2;
 this.data.studentId=this.studId2;
      this.authan.updateResult(this.data,this.studId, this.courseId,this.type,this.year).subscribe(
        resp => {

          if (resp.status != null) {
            console.log('dsd',resp.message);
            console.log('dsd',resp.status);
            switch (resp.status) {
              case 201:
                this.updateToggle=false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }
  }

}
class Datas {
  marks: string;
  grade: string;
  type: string;
  studentId: string;
  courseId: string;
  year: string;
}
