import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdStaffComponent } from './upd-staff.component';

describe('UpdStaffComponent', () => {
  let component: UpdStaffComponent;
  let fixture: ComponentFixture<UpdStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
