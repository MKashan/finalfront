import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-upd-staff',
  templateUrl: './upd-staff.component.html',
  styleUrls: ['./upd-staff.component.css']
})
export class UpdStaffComponent implements OnInit {

  toggle= false;
  message: string;
  data= {};
  staffID: number;
  Signal: boolean;
  updateToggle = false;
staff=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {

    this.authan.getStaffs().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.staff= responce.data;
              this.staffID=responce.data[0].id;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.staffID= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }

  getData(){
    this.authan.getStaff(this.staffID).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          console.log(responce.message);
          switch ( responce.status) {
            case 200:
              this.updateToggle = true;
              this.Signal = true;
              this.toggle = true;
              this.data = responce.data;

              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    if (this.Signal == true) {
      this.authan.updateStaff(this.data,this.staffID).subscribe(
        resp => {
          console.log('dasdsadadsa',resp.status)
          if (resp.status != null) {
            switch (resp.status) {
              case 201:
                this.updateToggle=false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }

  }
}
