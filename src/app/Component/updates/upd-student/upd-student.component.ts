import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-upd-student',
  templateUrl: './upd-student.component.html',
  styleUrls: ['./upd-student.component.css']
})
export class UpdStudentComponent implements OnInit {
  toggle= false;
  message: string;
  data = {}
  data2 = new Data();
  data3 = [];
  name: string;
  Signal: boolean;
  updateToggle = false;
  student=[];
  sectionName: string;
  class: string;
  campusId: string;
  sec:string;
  section=[];
  campus=[];
  camp:number;
  temp1:string;
  temp2:string;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getStudents().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:

              this.name=responce.data[0].id;
              this.student= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );

    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.section= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:

              this.campus= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );

  }
  on(deviceValue) {
    this.name= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.sec= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.camp= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }

  getData(){
    this.authan.getStudent(this.name).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.data);
          switch ( responce.status) {
            case 200:
              this.updateToggle = true;
              this.Signal = true;
              this.toggle = true;
              this.data = responce.data[0];
              this.data3 = responce.data;
              this.message = responce.message;
              this.camp=responce.data[0].section.secCampusId;
              this.sec=responce.data[0].section.name;
              this.temp2=responce.data[0].section.secCampusId;
              this.temp1=responce.data[0].section.name;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    this.data2.id=this.data3[0].id;
    this.data2.sectionName=this.sec;
    this.data2.class=this.data3[0].section.class;
    this.data2.campusId=this.camp;
    this.data2.firstName=this.data3[0].firstName;
    this.data2.lastName=this.data3[0].lastName;
    this.data2.age=this.data3[0].age;
    this.data2.email=this.data3[0].email;
    this.data2.address=this.data3[0].address;
    this.data2.contact=this.data3[0].contact;
    console.log("wahbhai",this.data2)
    if (this.Signal == true) {
      this.authan.updateStudent(this.data2).subscribe(
        resp => {
          if (resp.status != null) {
            console.log('osamassss',resp.message)
            console.log('osama555555555',resp.status)
            switch (resp.status) {
              case 201:
                this.updateToggle=false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }

  }
}
class Data{
  sectionName:string;
  class:string;
  campusId:number;
  firstName:string;
  lastName:string;
  age:number;
  contact:string;
  email:string;
  address:string;
  id:number;

}
