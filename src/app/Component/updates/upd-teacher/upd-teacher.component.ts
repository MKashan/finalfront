import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-upd-teacher',
  templateUrl: './upd-teacher.component.html',
  styleUrls: ['./upd-teacher.component.css']
})
export class UpdTeacherComponent implements OnInit {
  toggle= false;
  message: string;
  data={};
  name: string;
  Signal:boolean;
  updateToggle=false;
  teacher=[];
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    this.authan.getteachers().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log("teachers",responce.data)
          switch ( responce.status) {
            case 200:
              this.name=responce.data[0].id;
              this.teacher= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.name= deviceValue.target.value;

  }
  toggles(){
    this.toggle = false;
  }

  getData(){
    this.authan.getteacher(this.name).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              this.updateToggle = true;
               this.Signal = true;
              this.toggle = true;
              this.data = responce.data;
              console.log('osama', this.data)
              this.message = responce.message;
              break;
            case 403:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

  update() {
    if (this.Signal == true) {
      this.authan.updateTeacher(this.data, this.name).subscribe(
        resp => {
          if (resp.status != null) {
            switch (resp.status) {
              case 201:
                this.updateToggle = false;
                this.toggle = true;
                this.message = resp.message;
                break;
              case 403:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 404:
                this.toggle = true;
                this.message = resp.message;
                break;
              case 500:
                this.toggle = true;
                this.message = resp.message;
                break;
            }
          }
        }
      );
    }

  }
}
class Datas {
  firstName: string;
  lastName: string;
  age: string;
  contact: string;
  address: string;
  email: string;
  salary: string;
  qualification: string;
}
