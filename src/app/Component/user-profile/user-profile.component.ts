import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  admin=false;
  staff=false;
  teach=false;
  stud=false;
  constructor(private router: Router) { }

  ngOnInit() {

    if(localStorage.getItem('type') == 'loginStaff'){
      this.staff=true;
    }
    if(localStorage.getItem('type') == 'loginStudent'){
      this.stud=true;
    }
    if(localStorage.getItem('type') == 'loginAdmin'){
      this.admin=true;
    }
    if(localStorage.getItem('type') == 'loginTeacher'){
      this.teach=true;
    }
  }


  logout(){
    localStorage.clear();
    this.router.navigate((['login']));
  }

}
