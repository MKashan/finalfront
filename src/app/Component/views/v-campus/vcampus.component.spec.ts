import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VCampusComponent } from './vcampus.component';

describe('VCampusComponent', () => {
  let component: VCampusComponent;
  let fixture: ComponentFixture<VCampusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VCampusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VCampusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
