import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-v-teacher',
  templateUrl: './v-teacher.component.html',
  styleUrls: ['./v-teacher.component.css']
})
export class ViewTeacherComponent implements OnInit {
  value: string;
  responce= {};
  Data= {};
  courses=[];
  campus=[];
  toggle= false;
  message: string;
  teacher=[];
  constructor(private authan: GlobalServService) {


  }

  ngOnInit() {
    this.authan.getteachers().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log("teachers",responce.data)
          switch ( responce.status) {
            case 200:
              this.value=responce.data[0].id;
              this.teacher= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.value= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  show() {
    console.log('sdasd',this.value)
    this.authan.getteacher(this.value).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.data);
          switch ( responce.status) {
            case 200:
              console.log('sdsddadasdsad');
              this.toggle = true;
              this.Data = responce.data;
              this.courses=responce.data.courses;
              this.campus=responce.data.campuses;
              this.message= responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.Data = responce.data;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

}
