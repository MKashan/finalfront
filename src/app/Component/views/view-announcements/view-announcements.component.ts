import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-view-announcements',
  templateUrl: './view-announcements.component.html',
  styleUrls: ['./view-announcements.component.css']
})
export class ViewAnnouncementsComponent implements OnInit {

  value: string;
  responce= {};
  Data=[];
  toggle= false;
  message: string;
  campus=[];
  constructor(private authan: GlobalServService) {


  }

  ngOnInit() {
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.value=responce.data[0].id;
              this.campus= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.value= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  show() {
    this.authan.getAnnouc(this.value).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.data);
          switch ( responce.status) {
            case 200:
              console.log('sdsddadasdsad');
              this.toggle = true;
              this.Data = responce.data;
              this.message= responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.Data = responce.data;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }

}
