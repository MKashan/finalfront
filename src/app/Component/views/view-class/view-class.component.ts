import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-view-class',
  templateUrl: './view-class.component.html',
  styleUrls: ['./view-class.component.css']
})
export class ViewClassComponent implements OnInit {
  value: string;
  campusName:string;
  classes:string;
  sectionName:string;
  responce= {};
  Datas= [];
  toggle= false;
  message: string;
  campus=[];
  section=[];
  camp:string;
  sec:string;
  clas:string;
  constructor(private authan: GlobalServService) {


  }

  ngOnInit() {
    this.authan.getCampuses().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.camp=responce.data[0].id;
              this.campus= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.sec=responce.data[0].name;
              this.clas=responce.data[0].class;
              this.section= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );

  }
  on(deviceValue) {
    this.camp= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.sec= deviceValue.target.value;

  }
  on3(deviceValue) {
    this.clas= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  show() {
    this.authan.getSection(this.camp,this.sec,this.clas).subscribe(
      responce => {
        if (responce.status != null) {
          console.log(responce.message);
          switch (responce.status) {
            case 200:
              console.log(responce.status);
              this.toggle = true;
              this.Datas = responce.data;
              this.message = responce.message;
              break;
            case 404:
              this.toggle = true;
              this.Datas = responce.data;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.Datas = responce.data;
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }


}
