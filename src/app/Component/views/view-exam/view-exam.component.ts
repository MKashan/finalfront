import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-view-exam',
  templateUrl: './view-exam.component.html',
  styleUrls: ['./view-exam.component.css']
})
export class ViewExamComponent implements OnInit {

  value: string;
  responce= {};
  Datas= [];
  toggle= false;
  message: string;
  mouth:string;
  Day:string;
  year:string;
  temp=[];
  section=[];
  constructor( private authan: GlobalServService) {


  }

  ngOnInit() {
    this.authan.getSections().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.value=responce.data[0].id;
              this.section= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.value= deviceValue.target.value;
  }
  toggles(){
    this.toggle= false;
  }
  show() {
    this.authan.getExam(this.value).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.data);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.Datas = responce.data;
              this.message= responce.message;
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }

}
