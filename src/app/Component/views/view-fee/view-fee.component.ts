import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-view-fee',
  templateUrl: './view-fee.component.html',
  styleUrls: ['./view-fee.component.css']
})
export class ViewFeeComponent implements OnInit {
  studId: string;
  type: string;
  detail: string;
  responce= {};
  Datas= [];
  toggle= false;
  message: string;
  student=[];
  constructor( private authan: GlobalServService) {


  }

  ngOnInit() {
    this.authan.getStudents().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.studId=responce.data[0].id;
              this.student= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.studId= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  show() {
    this.authan.getFees(this.studId,this.type,this.detail).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.data);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.Datas = responce.data;
              this.message= responce.message;
              break;
            case 404:
              this.toggle = true;
              this.Datas = responce.data;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.Datas = responce.data;
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
}
