import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
import {document} from '@angular/platform-browser/src/facade/browser';
@Component({
  selector: 'app-view-result',
  templateUrl: './view-result.component.html',
  styleUrls: ['./view-result.component.css']
})
export class ViewResultComponent implements OnInit {
  studentId: string;
  courseId:string;
  type: string;
  year: string;
  responce= {};
  Datas= [];
  toggle= false;
  message: string;
  student=[];
  constructor(private authan: GlobalServService) {


  }

  ngOnInit() {
    this.year = '2009';
    this.authan.getStudents().subscribe(
      responce => {
        if ( responce.status != null) {
          switch ( responce.status) {
            case 200:
              this.studentId=responce.data[0].id;
              this.student= responce.data;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.studentId= deviceValue.target.value;

  }
  on2(deviceValue) {
    this.year= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  show() {
    this.authan.getResultoF(this.studentId,this.type,this.year).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.data);
          switch ( responce.status) {
            case 200:
              this.toggle = true;
              this.Datas = responce.data;
              this.message= responce.message;
              break;
            case 404:
              this.toggle = true;
              this.Datas = responce.data;
              this.message = responce.message;
              break;
            case 500:
              this.toggle = true;
              this.Datas = responce.data;
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
}
