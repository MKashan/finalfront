import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../../../Services/global-serv.service';
@Component({
  selector: 'app-view-staff',
  templateUrl: './view-staff.component.html',
  styleUrls: ['./view-staff.component.css']
})
export class ViewStaffComponent implements OnInit {

  value: string;
  responce= {};
  Data= {};
  campus=[];
  toggle= false;
  message: string;
  staff=[];
  constructor( private authan: GlobalServService) {


  }

  ngOnInit() {

    this.authan.getStaffs().subscribe(
      responce => {
        if ( responce.status != null) {
          console.log('data',responce.data)
          switch ( responce.status) {
            case 200:
              this.staff= responce.data;
              this.value=responce.data[0].id;
              break;
            case 404:
              this.message = responce.message;
              break;
            case 500:
              this.message = responce.message;
              break;
          }


        }
      }
    );
  }
  on(deviceValue) {
    this.value= deviceValue.target.value;

  }
  toggles() {
    this.toggle = false;
  }
  show() {
    this.authan.getStaff(this.value).subscribe(
      responce => {
        if ( responce.status != null) {
          console.log(responce.status);
          switch ( responce.status) {
            case 200:
              console.log(responce.data)
              this.toggle = true;
              this.Data= responce.data;
              this.campus=responce.data.campuses;
              console.log('sdsa',this.campus);
              this.message= responce.message
              break;
            case 404:
              this.toggle = true;
              this.message = responce.message
              break;
            case 500:
              this.toggle = true;
              this.message = responce.message
              break;
          }


        }
      }
    );
  }


}
