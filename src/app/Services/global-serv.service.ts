import { Injectable } from '@angular/core';
import {Http , Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {ActivatedRoute} from '@angular/router';

@Injectable()
export class GlobalServService {
  userdetails = new data();
  studendtDetails = new Data();
  teacherDetails = new Data2();
  staffOrAdminDetails = new Data3();
type: string;
ipAddress= 'localhost';

  constructor(private http: Http ) { }


  login( userId: string, password: string, type: string ) {
    let url = 'http://' + this.ipAddress + ':3000/' + type;
    let data = {
      id: parseInt(userId, 10),
      password: password
    }


    return this.http.post(url, data ).map(response => response.json());
  }


  addStaffInCamp(data) {
    let urls = 'http://' + this.ipAddress + ':3000/staff2campus';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.post( urls , data, option ).map(response => response.json());
  }
  deletStaffCamp(campId, staffId) {
    let urls = 'http://' + this.ipAddress + ':3000/staff2campus/' + campId + '/' + staffId;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())
  }
  getteachers(){
    let url = 'http://' + this.ipAddress + ':3000/teachers';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }
  getStudents(){
    let url = 'http://' + this.ipAddress + ':3000/students';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }
  getAdmins(){
    let url = 'http://' + this.ipAddress + ':3000/admins';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }
  getSections(){
    let url = 'http://' + this.ipAddress + ':3000/sections';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }
  getStaffs(){
    let url = 'http://' + this.ipAddress + ':3000/staff';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }
  getCourses() {
    let url = 'http://' + this.ipAddress + ':3000/courses';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }

  getCampuses() {
    let url = 'http://' + this.ipAddress + ':3000/campuses/';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }

  getAdmin(id) {
    let url = 'http://' + this.ipAddress + ':3000/admins/' + id;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }
  addAdmin(data) {

    // console.log("hey",localStorage.getItem('token'));
    let urls = 'http://' + this.ipAddress + ':3000/admins';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.post( urls , data, option ).map(response => response.json());
  }
  updateAdmin(data, id) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/admins/'+ id;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.put(url, data, option).map(response => response.json())
  }
  deleteAdmin(name) {
    let urls = 'http://' + this.ipAddress+ ':3000/admins/' + name ;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())

  }
  getAnnouc(id) {
    let url = 'http://' + this.ipAddress + ':3000/announcements/' + id;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }

  addAnnouc(data) {

    // console.log("hey",localStorage.getItem('token'));
    let urls = 'http://' + this.ipAddress + ':3000/announcements';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.post( urls , data, option ).map(response => response.json());
  }

  getStudent (name: string) {
    let url = 'http://' + this.ipAddress + ':3000/students/' + name;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.get(url, option).map(response => response.json());
  }
  addStud(data, sectionName, name) {

    // console.log("hey",localStorage.getItem('token'));
    let urls = 'http://' + this.ipAddress+ ':3000/students/'+ sectionName + '/' + name;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.post( urls , data, option ).map(response => response.json());
  }
  updateStudent(data) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/students';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.put(url, data, option).map(response => response.json())
  }
  deleteStudent(name) {
    let urls = 'http://' + this.ipAddress + ':3000/students/' + name ;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())

  }
  getCampus (name: string) {
    let url = 'http://' + this.ipAddress + ':3000/campuses/' + name;


    return this.http.get(url).map(response => response.json());
  }
addCampuses(data) {

    // console.log("hey",localStorage.getItem('token'));
  let urls = 'http://' + this.ipAddress + ':3000/campuses';
 let headers = new Headers();
  headers.append('token', localStorage.getItem('token'));
  let option = new RequestOptions({headers: headers});
  return this.http.post( urls , data, option ).map(response => response.json());
}
deleteCampus(name) {
    console.log( "sdsd" )
  let urls = 'http://' + this.ipAddress + ':3000/campuses/' + name ;
  let headers = new Headers();
  headers.append('token', localStorage.getItem('token'));
  let option = new RequestOptions({headers: headers});
  return this.http.delete( urls, option).map(response => response.json())

}

updateCampus(data, name) {
  console.log('update', localStorage.getItem('token'))
  let url = 'http://' + this.ipAddress + ':3000/campuses/' + name;
  let headers = new Headers();
  headers.append('token', localStorage.getItem('token'));
  let option = new RequestOptions({headers: headers});

  return this.http.put(url, data, option).map(response => response.json())
}

  getResult(studId,type,year,courseId) {
    let url = 'http://' + this.ipAddress + ':3000/results/' + studId + '/' + type + '/' + year + '/' + courseId;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.get( url, option ).map( response => response.json() );
  }
  getResultoF(studId, type, year) {
    let url = 'http://' + this.ipAddress + ':3000/results/' + studId + '/' + type + '/' + year;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.get( url, option ).map( response => response.json() );
  }


  addResult(data){
    let urls = 'http://' + this.ipAddress + ':3000/results';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.post( urls , data, option ).map(response => response.json());
  }
  updateResult(data, studId, courseId, type, year) {
  console.log('update', localStorage.getItem('token'))
  let url = 'http://' + this.ipAddress + ':3000/results/' + studId + '/' + courseId + '/' + type + '/' + year + '/';
  let headers = new Headers();
  headers.append('token', localStorage.getItem('token'));
  let option = new RequestOptions({headers: headers});
    return this.http.put(url, data, option).map(response => response.json())
}
  deleteResult(studentId, type, year, courseId) {

    let urls = 'http://' + this.ipAddress + ':3000/results/' + studentId + '/' + type + '/' + year + '/' + courseId;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())
  }

  getFees(studId, type, detail) {
    let url = 'http://' + this.ipAddress + ':3000/fees/' + studId + '/' + type + '/' + detail;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.get( url, option ).map( response => response.json() );
  }
  addFees(data) {

    // console.log("hey",localStorage.getItem('token'));
    let urls = 'http://' + this.ipAddress + ':3000/fees';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.post( urls , data, option ).map(response => response.json());
  }

  updateFees(data, studId, type, detail) {
    let url = 'http://' + this.ipAddress + ':3000/fees/' + studId + '/' + type + '/' + detail;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.put(url, data, option).map(response => response.json())
  }
  deletefee(studId, type, detail) {

    let urls = 'http://' + this.ipAddress + ':3000/fees/' + studId + '/' + type + '/' + detail;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())
  }

   getExam(secId){
     let url = 'http://' + this.ipAddress + ':3000/examsBySection/' + secId;
     let headers = new Headers();
     headers.append('token', localStorage.getItem('token'));
     let option = new RequestOptions({headers: headers});
     return this.http.get( url, option ).map( response => response.json() );
   }
  getExams(secId, courseId) {
    let url = 'http://' + this.ipAddress + ':3000/exams/' + secId + '/' + courseId;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.get( url, option ).map( response => response.json() );
  }
  deleteExam(secId, courseId) {

    let urls = 'http://' + this.ipAddress + ':3000/exams/' + secId + '/' + courseId;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())
  }
  updateExam(data, courseId, secId) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/exams/' + secId + '/' + courseId;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.put(url, data, option).map(response => response.json())
  }
  addExam(data) {

    // console.log("hey",localStorage.getItem('token'));
    let urls = 'http://' + this.ipAddress + ':3000/exams';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.post( urls , data, option ).map(response => response.json());
  }

  getSection(campusName, sectionName, classes) {
    let url = 'http://' + this.ipAddress + ':3000/sections/' + campusName + "/" + sectionName + "/" + classes;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.get( url, option ).map( response => response.json() );
  }

  addSection(data) {

    // console.log("hey",localStorage.getItem('token'));
    let urls = 'http://' + this.ipAddress + ':3000/sections';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.post( urls , data, option ).map(response => response.json());
  }
  deleteSection(campId, secId, classes) {

    let urls = 'http://' + this.ipAddress + ':3000/sections/' + campId + '/' + secId + '/' + classes;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())

  }
  updateSection(data, campId, secId, classes) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/sections/' + campId + '/' + secId + '/' + classes;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.put(url, data, option).map(response => response.json())
  }

  getStaff(name) {
    let url = 'http://' + this.ipAddress + ':3000/staff/' + name;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.get( url, option ).map( response => response.json() );
  }
  addStaff(data) {
    console.log(data)
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/staff';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.post(url, data, option).map(response => response.json())
  }
  updateStaff(data, name) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/staff/' + name;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.put(url, data, option).map(response => response.json())
  }
  deleteStaff(name) {
    console.log( "DeleteCAmpus LOG" )
    let urls = 'http://' + this.ipAddress + ':3000/staff/' + name ;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())

  }
  getteacher (name) {
   console.log('pro', name)
    let url = 'http://' + this.ipAddress + ':3000/teachers/' + name;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.get( url, option ).map( response => response.json() );
  }

  deleteTeacher(name) {
    console.log( "DeleteCAmpus LOG" )
    let urls = 'http://' + this.ipAddress + ':3000/teachers/' + name ;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())

  }

  updateTeacher(data, name) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/teachers/' + name;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.put(url, data, option).map(response => response.json())
  }

  addTeacher(data) {
    console.log(data)
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/teachers';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.post(url, data, option).map(response => response.json())
  }
deletTeachCamp(teachid, campid) {
  let urls = 'http://' + this.ipAddress + ':3000/teacher2campus/' + campid + '/' + teachid ;
  let headers = new Headers();
  headers.append('token', localStorage.getItem('token'));
  let option = new RequestOptions({headers: headers});
  return this.http.delete( urls, option).map(response => response.json())
}
  addTeacherInCamp(data) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/teacher2campus';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.post(url, data, option).map(response => response.json())
  }
  deletTeachCourse(teachid, classid, courseName) {
    let urls = 'http://' + this.ipAddress + ':3000/teacher2course/' + courseName + '/' + classid + '/' + teachid ;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())
  }
  addTeacherInCourse(data) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/teacher2course';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.post(url, data, option).map(response => response.json())
  }


  deletCourseInSec(sectionName, classid, courseName) {
    let urls = 'http://' + this.ipAddress + ':3000/course2section/' + courseName + '/' + sectionName + '/' + classid ;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())
  }
  addCourseInSec(data) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/course2section';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.post(url, data, option).map(response => response.json())
  }

  addCourse(data) {
    console.log(data)
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/courses';
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.post(url, data, option).map(response => response.json())
  }
  getCourse(name, classes) {
    console.log('pro', name)
    let url = 'http://' + this.ipAddress + ':3000/courses/' + name + '/' + classes;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.get( url, option ).map( response => response.json() );
  }
  updateCourse(name, data, classes) {
    console.log('update', localStorage.getItem('token'))
    let url = 'http://' + this.ipAddress + ':3000/courses/' + name + '/' + classes;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});

    return this.http.put(url, data, option).map(response => response.json())
  }
  deleteCourse(name, classes) {
    console.log( "DeleteCAmpus LOG" )
    let urls = 'http://' + this.ipAddress + ':3000/courses/' + name+ '/' + classes ;
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let option = new RequestOptions({headers: headers});
    return this.http.delete( urls, option).map(response => response.json())

  }


}
class data {
  id: number;
}
class Data {
  firstName: string;
  lastName: string;
  age: number;
  email: string;
  gender: number;
  contact: number;
  section: {};
}
class Data2 {
  firstName: string;
  lastName: string;
  age: number;
  email: string;
  gender: number;
  contact: number;
  campuses: string [];
  courses: string [];
}
class Data3 {
  firstName: string;
  lastName: string;
  age: number;
  email: string;
  gender: number;
  contact: number;
  campuses: string [];
}
