import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from '../../src/app/routes';

import {AddAnnouncementsComponent} from './Component/add-announcements/add-announcements.component';
import {ViewAnnouncementsComponent} from './Component/views/view-announcements/view-announcements.component';

import { LoginComponent } from './Component/login/login.component';
import { HomeComponent } from './home/home.component';
import { CourseComponent } from './Component/course/course.component';
import { CourseAddComponent } from './Component/course-add/course-add.component';
import { StudentComponent } from './Component/student/student.component';
import { AddStudentComponent } from './Component/add-student/add-student.component';
import { AddTeacherComponent } from './Component/add-teacher/add-teacher.component';
import { TeacherComponent } from './Component/teacher/teacher.component';
import { StaffComponent } from './Component/staff/staff.component';
import { AddStaffComponent } from './Component/add-staff/add-staff.component';
import { CampusComponent } from './Component/campus/campus.component';
import { AddCampusComponent } from './Component/add-campus/add-campus.component';
import { ExamComponent } from './Component/exam/exam.component';
import { AddExamComponent } from './Component/add-exam/add-exam.component';
import { ResultComponent } from './Component/result/result.component';
import { AddResultComponent } from './Component/add-result/add-result.component';
import { UserProfileComponent } from './Component/user-profile/user-profile.component';
import { ClassComponent } from './Component/class/class.component';
import { AddClassComponent } from './Component/add-class/add-class.component';
import { ViewTeacherComponent } from './Component/views/v-teacher/viewTeacher.component';




import {StudentService} from '../../src/app/Services/student.service'
import {GlobalServService} from '../../src/app/Services/global-serv.service';
import { VCampusComponent } from './Component/views/v-campus/vcampus.component';
import { ViewCourseComponent } from './Component/views/view-course/view-course.component';
import { ViewStaffComponent } from './Component/views/view-staff/view-staff.component';
import { ViewStudentComponent } from './Component/views/view-student/view-student.component';
import { ViewClassComponent } from './Component/views/view-class/view-class.component';
import { ViewExamComponent } from './Component/views/view-exam/view-exam.component';
import { ViewResultComponent } from './Component/views/view-result/view-result.component';
import { FeesComponent } from './Component/fees/fees.component';
import { AddFeeComponent } from './Component/add-fee/add-fee.component';
import { ViewFeeComponent } from './Component/views/view-fee/view-fee.component';
import { DelCampusComponent } from './Component/Delete/del-campus/del-campus.component';
import { DelCourseComponent } from './Component/Delete/del-course/del-course.component';
import { DelTeacherComponent } from './Component/Delete/del-teacher/del-teacher.component';
import { DelStaffComponent } from './Component/Delete/del-staff/del-staff.component';
import { DelStudentComponent } from './Component/Delete/del-student/del-student.component';
import { DelClassComponent } from './Component/Delete/del-class/del-class.component';
import { DelExameComponent } from './Component/Delete/del-exame/del-exame.component';
import { DelResultComponent } from './Component/Delete/del-result/del-result.component';
import { DelFeeComponent } from './Component/Delete/del-fee/del-fee.component';
import { UpdCampusComponent } from './Component/updates/upd-campus/upd-campus.component';
import { UpdTeacherComponent } from './Component/updates/upd-teacher/upd-teacher.component';
import { UpdCourseComponent } from './Component/updates/upd-course/upd-course.component';
import { UpdStaffComponent } from './Component/updates/upd-staff/upd-staff.component';
import { UpdStudentComponent } from './Component/updates/upd-student/upd-student.component';
import { UpdClassComponent } from './Component/updates/upd-class/upd-class.component';
import { UpdExamComponent } from './Component/updates/upd-exam/upd-exam.component';
import { TeacherInCampComponent } from './Component/teacherFunc/teacher-in-camp/teacher-in-camp.component';
import { TeacherInCourseComponent } from './Component/teacherFunc/teacher-in-course/teacher-in-course.component';
import { CourseInSectionComponent } from './Component/courseFunc/course-in-section/course-in-section.component';
import { UpdResultComponent } from './Component/updates/upd-result/upd-result.component';
import { UpdFeeComponent } from './Component/updates/upd-fee/upd-fee.component';
import { AnnouncementsComponent } from './Component/announcements/announcements.component';
import { AdminComponent } from './Component/admin/admin.component';
import { AddAdminComponent } from './Component/add-admin/add-admin.component';
import { ViewAdminComponent } from './Component/views/view-admin/view-admin.component';
import { UpdAdminComponent } from './Component/updates/upd-admin/upd-admin.component';
import { DelAdminComponent } from './Component/Delete/del-admin/del-admin.component';
import { StaffToCampusComponent } from './Component/staffFunc/staff-to-campus/staff-to-campus.component';
import { ViewProfileComponent } from './Component/views/view-profile/view-profile.component';
import { AdminProfComponent } from './Component/profiles/admin-prof/admin-prof.component';
import { StudentProfComponent } from './Component/profiles/student-prof/student-prof.component';
import { StaffProfComponent } from './Component/profiles/staff-prof/staff-prof.component';
import { TeacherProfComponent } from './Component/profiles/teacher-prof/teacher-prof.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AnnouncementsComponent,
    AddAnnouncementsComponent,
    ViewAnnouncementsComponent,
    CourseComponent,
    CourseAddComponent,
    StudentComponent,
    AddStudentComponent,
    AddTeacherComponent,
    TeacherComponent,
    StaffComponent,
    AddStaffComponent,
    CampusComponent,
    AddCampusComponent,
    ExamComponent,
    AddExamComponent,
    ResultComponent,
    AddResultComponent,
    UserProfileComponent,
    ClassComponent,
    AddClassComponent,
    ViewTeacherComponent,
    VCampusComponent,
    ViewCourseComponent,
    ViewStaffComponent,
    ViewStudentComponent,
    ViewClassComponent,
    ViewExamComponent,
    ViewResultComponent,
    FeesComponent,
    AddFeeComponent,
    ViewFeeComponent,
    DelCampusComponent,
    DelCourseComponent,
    DelTeacherComponent,
    DelStaffComponent,
    DelStudentComponent,
    DelClassComponent,
    DelExameComponent,
    DelResultComponent,
    DelFeeComponent,
    UpdCampusComponent,
    UpdTeacherComponent,
    UpdCourseComponent,
    UpdStaffComponent,
    UpdStudentComponent,
    UpdClassComponent,
    UpdExamComponent,
    TeacherInCampComponent,
    TeacherInCourseComponent,
    CourseInSectionComponent,
    UpdResultComponent,
    UpdFeeComponent,
    AdminComponent,
    AddAdminComponent,
    ViewAdminComponent,
    UpdAdminComponent,
    DelAdminComponent,
    StaffToCampusComponent,
    ViewProfileComponent,
    AdminProfComponent,
    StudentProfComponent,
    StaffProfComponent,
    TeacherProfComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,


    routing
  ],
  providers: [GlobalServService],
  bootstrap: [AppComponent]
})
export class AppModule { }
