import { Component, OnInit } from '@angular/core';
import {GlobalServService} from '../Services/global-serv.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  admin=true;
  announcements=true;
  campus= true;
  teacher= true;
  course= true;
  staff= true;
  student= true;
  class= true;
  exam= true;
  result= true;
  profile= true;
  fees=true;
  constructor(private authan: GlobalServService) { }

  ngOnInit() {
    if(localStorage.getItem('type') == 'loginStudent'){
       this.teacher=false;
       this.staff=false;
   this.student=false;
   this.admin=false;
    }

    if(localStorage.getItem('type') == 'loginTeacher'){
      this.student=false;
      this.staff=false;
      this.teacher=false;
      this.admin=false;
      this.result=false;
      this.fees=false;
    }

    if(localStorage.getItem('type') == 'loginStaff'){
      this.staff=true;
      this.admin=false;
    }

  }

}
