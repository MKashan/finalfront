/**
 * Created by Arsalan on 4/26/2017.
 */

import { ModuleWithProviders } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';

import {AnnouncementsComponent} from './Component/announcements/announcements.component';
import {AddAnnouncementsComponent} from './Component/add-announcements/add-announcements.component';
import {ViewAnnouncementsComponent} from './Component/views/view-announcements/view-announcements.component';


import {AdminComponent} from './Component/admin/admin.component';
import {UpdAdminComponent} from './Component/updates/upd-admin/upd-admin.component';
import {ViewAdminComponent} from './Component/views/view-admin/view-admin.component';
import {DelAdminComponent} from './Component/Delete/del-admin/del-admin.component';
import {AddAdminComponent} from './Component/add-admin/add-admin.component';

import {CampusComponent} from './Component/campus/campus.component';
import {AddCampusComponent} from './Component/add-campus/add-campus.component';
import {VCampusComponent} from './Component/views/v-campus/vcampus.component';
import {DelCampusComponent} from './Component/Delete/del-campus/del-campus.component';
import {UpdCampusComponent} from './Component/updates/upd-campus/upd-campus.component';

import {TeacherComponent} from './Component/teacher/teacher.component';
import {AddTeacherComponent} from './Component/add-teacher/add-teacher.component';
import {ViewTeacherComponent} from './Component/views/v-teacher/viewTeacher.component';
import {DelTeacherComponent} from './Component/Delete/del-teacher/del-teacher.component';
import {UpdTeacherComponent} from './Component/updates/upd-teacher/upd-teacher.component';
import {TeacherInCampComponent} from './Component/teacherFunc/teacher-in-camp/teacher-in-camp.component';
import {TeacherInCourseComponent} from './Component/teacherFunc/teacher-in-course/teacher-in-course.component';

import {CourseComponent} from './Component/course/course.component';
import {CourseAddComponent} from './Component/course-add/course-add.component';
import {ViewCourseComponent} from './Component/views/view-course/view-course.component';
import {DelCourseComponent} from './Component/Delete/del-course/del-course.component';
import {UpdCourseComponent} from './Component/updates/upd-course/upd-course.component';
import {CourseInSectionComponent} from './Component/courseFunc/course-in-section/course-in-section.component';

import {StaffComponent} from './Component/staff/staff.component';
import {AddStaffComponent} from './Component/add-staff/add-staff.component';
import {ViewStaffComponent} from './Component/views/view-staff/view-staff.component';
import {DelStaffComponent} from './Component/Delete/del-staff/del-staff.component';
import {UpdStaffComponent} from './Component/updates/upd-staff/upd-staff.component';
import {StaffToCampusComponent} from './Component/staffFunc/staff-to-campus/staff-to-campus.component';

import {StudentComponent} from './Component/student/student.component';
import {ViewStudentComponent} from './Component/views/view-student/view-student.component';
import {AddStudentComponent} from './Component/add-student/add-student.component';
import {DelStudentComponent} from './Component/Delete/del-student/del-student.component';
import {UpdStudentComponent} from './Component/updates/upd-student/upd-student.component';

import {ClassComponent} from './Component/class/class.component';
import {ViewClassComponent} from './Component/views/view-class/view-class.component';
import {AddClassComponent} from './Component/add-class/add-class.component';
import {DelClassComponent} from './Component/Delete/del-class/del-class.component';
import {UpdClassComponent} from './Component/updates/upd-class/upd-class.component';

import {ExamComponent} from './Component/exam/exam.component';
import {AddExamComponent} from './Component/add-exam/add-exam.component';
import {ViewExamComponent} from './Component/views/view-exam/view-exam.component';
import {DelExameComponent} from './Component/Delete/del-exame/del-exame.component';
import {UpdExamComponent} from './Component/updates/upd-exam/upd-exam.component';

import {ResultComponent} from './Component/result/result.component';
import {AddResultComponent} from './Component/add-result/add-result.component';
import {ViewResultComponent} from './Component/views/view-result/view-result.component';
import {DelResultComponent} from './Component/Delete/del-result/del-result.component';
import {UpdResultComponent} from './Component/updates/upd-result/upd-result.component';



import {FeesComponent} from './Component/fees/fees.component';
import {AddFeeComponent} from './Component/add-fee/add-fee.component';
import {ViewFeeComponent} from './Component/views/view-fee/view-fee.component';
import {DelFeeComponent} from './Component/Delete/del-fee/del-fee.component';
import {UpdFeeComponent} from './Component/updates/upd-fee/upd-fee.component';



import {UserProfileComponent} from './Component/user-profile/user-profile.component';
import{AdminProfComponent} from'./Component/profiles/admin-prof/admin-prof.component';
import{StaffProfComponent} from'./Component/profiles/staff-prof/staff-prof.component';
import{StudentProfComponent} from'./Component/profiles/student-prof/student-prof.component';
import{TeacherProfComponent} from'./Component/profiles/teacher-prof/teacher-prof.component';


import {LoginComponent} from './Component/login/login.component';






 // import {AddTeacherComponent} from './Component/add-teacher/add-teacher.component';
// {path:'addExam',component:AddExamComponent},


// {path:'addStaff',component:AddStaffComponent},

// {path:'addTeacher',component:AddTeacherComponent},
// {path:'campus',component:campusComponent},




//  {path:'course',component:CourseComponent}
// {path:'Component/course-add',component:LoginComponent}

// *****************Route Configuration

export const routes: Routes = [

 ////Child Routing is done here//
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent}, { path: 'home', component: HomeComponent, children: [

    {path: 'announcements', component: AnnouncementsComponent, children: [{path: 'addAnnouncements', component: AddAnnouncementsComponent},
      {path: 'viewAnnouncements', component: ViewAnnouncementsComponent}]},


    {path: 'admin', component: AdminComponent, children: [{path: 'viewAdmin', component: ViewAdminComponent},
      {path: 'delAdmin', component: DelAdminComponent},
      {path: 'addAdmin', component: AddAdminComponent},
      {path: 'updAdmin', component: UpdAdminComponent}]},

    {path: 'campus', component: CampusComponent, children: [{path: 'addCampus', component: AddCampusComponent},
  {path: 'vCampus', component: VCampusComponent},
    {path: 'delCampus', component: DelCampusComponent},
      {path: 'updCampus', component: UpdCampusComponent}]},

  {path: 'teacher', component: TeacherComponent, children: [{path: 'addTeacher', component: AddTeacherComponent},
    {path: 'ViewTeacher', component: ViewTeacherComponent},
    {path: 'delTeacher', component: DelTeacherComponent},
    {path: 'updTeacher', component: UpdTeacherComponent},
    {path: 'teacherInCamp', component: TeacherInCampComponent},
    {path: 'teacherInCourse', component: TeacherInCourseComponent}]},

  {path: 'course', component: CourseComponent, children: [{path: 'courseAdd', component: CourseAddComponent},
    {path: 'viewCourse', component: ViewCourseComponent},
    {path: 'delCourse', component: DelCourseComponent},
    {path: 'updCourse', component: UpdCourseComponent},
    {path: 'courseInSection', component: CourseInSectionComponent}]},

  {path: 'staff', component: StaffComponent, children: [{path: 'addStaff', component: AddStaffComponent },
    {path: 'viewStaff', component: ViewStaffComponent},
    {path: 'delStaff', component: DelStaffComponent},
    {path: 'updStaff', component: UpdStaffComponent},
    {path: 'staffToCampus', component: StaffToCampusComponent}]},

  {path: 'student', component: StudentComponent, children: [{path: 'addStudent', component: AddStudentComponent },
    {path: 'ViewStudent', component: ViewStudentComponent},
    {path: 'delStudent', component: DelStudentComponent},
    {path: 'updStudent', component: UpdStudentComponent}]},

  {path: 'class', component: ClassComponent, children: [{path: 'addClass', component: AddClassComponent },
    {path: 'viewClass', component: ViewClassComponent},
    {path: 'delClass', component: DelClassComponent},
    {path: 'updClass', component: UpdClassComponent}]},


  {path: 'exam', component: ExamComponent, children: [{path: 'addExam', component: AddExamComponent },
    {path: 'viewExam', component: ViewExamComponent},
    {path: 'delExam', component: DelExameComponent},
    {path: 'updExam', component: UpdExamComponent}]},

  {path: 'result', component: ResultComponent, children: [{path: 'addResult', component: AddResultComponent },
    {path: 'viewResult', component: ViewResultComponent},
    {path: 'delResult', component: DelResultComponent},
    {path: 'updResult', component: UpdResultComponent}]},

    {path: 'Fee', component: FeesComponent, children: [{path: 'addFee', component: AddFeeComponent},
      {path: 'viewFee', component: ViewFeeComponent},
      {path: 'delFee', component: DelFeeComponent},
      {path: 'updFee', component: UpdFeeComponent}]},


    {path: 'userProfile', component: UserProfileComponent, children:[{path: 'adminProf', component: AdminProfComponent},
      {path: 'staffprof', component: StaffProfComponent},
      {path: 'studentProf', component: StudentProfComponent},
      {path: 'techerProf', component: TeacherProfComponent}]}

    ]}

];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
